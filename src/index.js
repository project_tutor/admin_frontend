import React from 'react';
import ReactDOM from 'react-dom';

// Apollo
import { ApolloProvider } from '@apollo/react-hooks';

// Redux
import { Provider as ReduxProvider } from 'react-redux';
import { createStore } from 'redux';
import client from './tools/apollo/index';
import App from './App';
import reducers from './tools/redux/reducers';

const store = createStore(reducers);

ReactDOM.render(
  <ApolloProvider client={client}>
    <ReduxProvider store={store}>
      <App />
    </ReduxProvider>
  </ApolloProvider>,
  document.getElementById('root'),
);
