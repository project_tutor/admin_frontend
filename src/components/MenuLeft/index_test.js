import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Icon, Layout } from 'antd';

import * as actions from '../../tools/redux/actions';
import './style.css';

const { Header, Sider, Content } = Layout;

const Index = (props) => {
  const { children, handleSaveUser, textSelected } = props;
  const history = useHistory();
  const [collapsed, setCollapsed] = useState(false);

  const handleClick = (e) => {
    // console.log('click ', e);
    const { key } = e;
    if (key === 'home') {
      history.push('/');
    } else if (key === 'user') {
      history.push('/user');
    } else if (key === 'logout') {
      localStorage.removeItem('token');
      handleSaveUser(null);
    }
  };

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          onClick={handleClick}
          defaultSelectedKeys={textSelected}
          style={{ height: '100vh' }}
        >
          <Menu.Item key="home">
            {/* <p>
              <Avatar size={30} src='https://baosongngu.vn/wp-content/uploads/2019/01/icon-devil.png' />
              <span style={{ fontWeight: 'bold' }}> Uber Teacher</span>
            </p> */}
            <Icon type="home" />
            <span>Home page</span>
          </Menu.Item>
          <Menu.Item key="user">
            <Icon type="user" />
            <span>Tài khoản</span>
          </Menu.Item>
          <Menu.Item key="logout">
            <Icon type="logout" />
            <span>Đăng xuất</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }}>
          <Icon
            className="trigger"
            type={collapsed ? 'menu-unfold' : 'menu-fold'}
            onClick={() => setCollapsed(!collapsed)}
            style={{ marginLeft: 25 }}
          />
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            background: '#fff',
            minHeight: 280,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => {
    dispatch(actions.saveUser(user));
  },
});

export default connect(null, mapDispatchToProps)(Index);
