import React from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Menu, Icon, Avatar, PageHeader } from 'antd';

import * as actions from '../../tools/redux/actions';

const Index = (props) => {
  const { children, handleSaveUser, textSelected } = props;
  const history = useHistory();

  const handleClick = (e) => {
    // console.log('click ', e);
    const { key } = e;
    if (key === 'home') {
      history.push('/');
    } else if (key === 'tag') {
      history.push('/tag');
    } else if (key === 'complaint') {
      history.push('/complaint');
    } else if (key === 'contract') {
      history.push('/contract');
    } else if (key === 'charts') {
      history.push('/charts');
    } else if (key === 'toprevenue') {
      history.push('/toprevenue');
    } else if (key === 'logout') {
      localStorage.removeItem('token');
      handleSaveUser(null);
    }
  };

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'flex-start' }} >
        <div style={{ width: '256px' }}>
          <Menu
            onClick={handleClick}
            defaultSelectedKeys={textSelected}
            mode="inline"
            theme="dark"
            style={{ height: '100vh' }}
          >
            <p style={{ marginTop: 10 }}>
                <Avatar size={40} src='https://baosongngu.vn/wp-content/uploads/2019/01/icon-devil.png' />
                <span style={{ fontWeight: 'bold' }}> Uber Teacher</span>
            </p>
            <Menu.Item key="home">
              <Icon type="user" />
              <span>Tài khoản</span>
            </Menu.Item>
            <Menu.Item key="tag">
              <Icon type="tag" />
              <span>Kỹ năng</span>
            </Menu.Item>
            <Menu.Item key="complaint">
              <Icon type="book" />
              <span>Khiếu nại</span>
            </Menu.Item>
            <Menu.Item key="contract">
              <Icon type="book" />
              <span>Hợp đồng</span>
            </Menu.Item>
            <Menu.Item key="charts">
              <Icon type="area-chart" />
              <span>Thống kê</span>
            </Menu.Item>
            <Menu.Item key="toprevenue">
              <Icon type="area-chart" />
              <span>Top doanh thu</span>
            </Menu.Item>
            <Menu.Item key="logout">
              <Icon type="logout" />
              <span>Đăng xuất</span>
            </Menu.Item>
          </Menu>
        </div>
        <div style={{ width: '100%' }}>
          <PageHeader
            style={{
              border: '1px solid rgb(235, 237, 240)',
              background: '#F0F2F5',
            }}
          />
          <div style={{ padding: '15px' }}>
            {children}
          </div>
        </div>
      </div >
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => {
    dispatch(actions.saveUser(user));
  },
});

export default connect(null, mapDispatchToProps)(Index);
