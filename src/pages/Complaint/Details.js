import React, { useState } from 'react';
import {
  Table, Tag, Modal, Button, Form, Row, Col, notification, Select, Spin,
} from 'antd';
import { gql } from 'apollo-boost';
import { useQuery, useMutation } from '@apollo/react-hooks';

const { Option } = Select;
const GET_COMPLAINT = gql`
    query  {
      complaints{
        _id
      contract{
        _id
        title
        feeHourly
        timeStudy
        teacher{
          _id
          money
        }
        student{
          _id
          money
        }
      }
      studentMessage
      teacherMessage
      state
      whoAccepted
      }
}
`;
const UPDATE_WIN_COMPLANT = gql`
 mutation UpdateComplaint($complaintId:ID!,$whoAccepted:WHOACCEPTED_COMPLAINT!){
  updateComplaint(complaintId:$complaintId,whoAccepted:$whoAccepted)
 }
`;
const UPDATE_MONEY = gql`
  mutation EditUser($userID:ID!,$input:UserInputEdit!){
    editUser(userID:$userID,input:$input)
  }
`;
const UPDATE_CONTRACT = gql`
  mutation UpdateContract($contractid:ID!,$input:CONTRACT_INPUT!){
    updateContract(contractid:$contractid,input:$input)
  }
`;
function getColorTag(text) {
  switch (text) {
    case 'PENDING':
      return 'GREEN';
    case 'AGREE':
      return 'LIME';
    case 'DISAGREE':
      return 'RED';
    case 'FINISH':
      return 'BLUE';
    case 'PROCESSED':
      return 'RED';
    default:
      break;
  }
}
const Detail = (props) => {
  const { getFieldDecorator } = props.form;
  const { loading: loadingDataComplaints, data: dataComplaints, refetch: reDataComplaints } = useQuery(GET_COMPLAINT, { fetchPolicy: 'network-only' });
  const [updateComplaint] = useMutation(UPDATE_WIN_COMPLANT);
  const [editUser] = useMutation(UPDATE_MONEY);
  const [updateContract] = useMutation(UPDATE_CONTRACT);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [idx, setIdx] = useState({});
  const showModal = () => {
    setVisible(true);
  };
  const handleCancel = () => {
    console.log('Clicked cancel button');
    setIdx(null);
    setVisible(false);
  };
  console.log(loadingDataComplaints);
  console.log(dataComplaints);
  const ConformStudentWin = async (values) => {
    const money = parseInt(idx.contract.timeStudy, 10) * parseInt(idx.contract.feeHourly, 10);
    await updateComplaint({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        complaintId: idx._id,
        whoAccepted: values.whowin,
      },
    });
    await editUser({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        userID: idx.contract.student._id,
        input: {
          money: (parseInt(idx.contract.student.money, 10) + money).toString(),
        },
      },
    });
    await updateContract({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        contractid: idx.contract._id,
        input: {
          state: 'STUDENTWIN',
          finishAt: Date(Date.now()).toString(),
        },
      },
    });
    setVisible(false);
    setConfirmLoading(false);
    setIdx(null);
    reDataComplaints();
    notification.success({
      message: 'Phản hồi thành công',
    });
  };
  const ConformTeachertWin = async (values) => {
    const money = parseInt(idx.contract.timeStudy, 10) * parseInt(idx.contract.feeHourly, 10);
    let sum = 0;
    if (!idx.contract.teacher.money) {
      sum = 0 + money;
    } else {
      sum = parseInt(idx.contract.teacher.money, 10) + money;
    }
    await updateComplaint({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        complaintId: idx._id,
        whoAccepted: values.whowin,
      },
    });
    await editUser({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        userID: idx.contract.teacher._id,
        input: {
          money: sum.toString(),
        },
      },
    });
    await updateContract({
      variables: {
        // eslint-disable-next-line no-underscore-dangle
        contractid: idx.contract._id,
        input: {
          state: 'TEACHERWIN',
          finishAt: Date.now().toString(),
        },
      },
    });
    setVisible(false);
    setConfirmLoading(false);
    setIdx(null);
    reDataComplaints();
    notification.success({
      message: 'Phản hồi thành công',
    });
  };
  const handleOk = async (events) => {
    events.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setConfirmLoading(true);
        if (values.whowin === 'STUDENT') {
          ConformStudentWin(values);
        } else {
          ConformTeachertWin(values);
        }
      } else {
        notification.error({
          message: 'Vui lòng nhập thông tin',
        });
      }
    });
  };
  const columns = [
    {
      title: 'Trạng thái',
      dataIndex: 'state',
      width: 150,
      // eslint-disable-next-line arrow-body-style
      render: (text) => (
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          <Tag color={getColorTag(text)} key={text}>
            {text.toUpperCase()}
          </Tag>
        </div>
      ),
    },
    {
      title: 'Tên hợp đồng',
      dataIndex: 'contract.title',
    },
    {
      title: 'Phản hồi từ học sinh',
      dataIndex: 'studentMessage',
    },
    {
      title: 'Phản hồi từ giáo viên',
      dataIndex: 'teacherMessage',
    }, {
      title: 'Người khiếu nại thành công',
      dataIndex: 'whoAccepted',
      render: (text) => (
        // eslint-disable-next-line no-unused-expressions
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          {text ? (
            <Tag color="#2db7f5" key={text}>
              {text.toUpperCase()}
            </Tag>
          ) : (
              <div />
              // eslint-disable-next-line indent
            )}
        </div>
      ),
    },
    {
      title: 'Tác vụ',
      key: 'action',
      // fixed: 'right',
      width: 120,
      render: (item) => (
        <span>
          {item.state === 'PENDING'
            && <Button type="link" size="small"
              onClick={() => {
                const temp = item;
                temp.IsCheck = true;
                setIdx(temp);
                showModal();
              }} >
              Phản hồi
            </Button>
          }
        </span>

      ),
    },
  ];
  return (
    <div>
      <div>
        {!loadingDataComplaints ? (
          <Table
            columns={columns}
            dataSource={dataComplaints ? dataComplaints.complaints : null}
            bordered
            rowKey='_id'
            title={() => (
              <h3>Danh sách khiếu nại</h3>
            )}
            scroll={{ y: 500 }}
          />
        ) : <Spin />}

      </div>
      <Modal
        title="Người khiếu nại thành công"
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Form layout="vertical" hideRequiredMark>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item >
                {getFieldDecorator('whowin', {
                  rules: [
                    {
                      required: true,
                      message: 'Nhập thông tin muốn phản hồi',
                    },
                  ],
                })(
                  <Select>
                    <Option value="STUDENT">Học sinh</Option>
                    <Option value="TEACHER">Giáo viên </Option>
                  </Select>,
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};
const App = Form.create()(Detail);
export default App;
