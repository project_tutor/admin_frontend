/* eslint-disable import/no-named-as-default */
import React from 'react';
// eslint-disable-next-line import/no-named-as-default-member
import MenuLeft from '../../components/MenuLeft/index';
import Details from './Details';

const ComPlaint = () => <MenuLeft textSelected='complaint'><Details /></MenuLeft>;

export default ComPlaint;
