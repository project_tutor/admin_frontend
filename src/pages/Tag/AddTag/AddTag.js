/* eslint-disable no-underscore-dangle */
import React from 'react';
import { Drawer, Form, Button, Row, Input, Select, notification } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const { Option } = Select;

const ADD_TAG = gql`
  mutation AddTag($input: TagInput!) {
    addTag(input: $input)
  }
`;

const EDIT_TAG = gql`
  mutation EditTag($tagID: ID!, $input: TagInput!) {
    editTag(tagID:$tagID,input: $input)
  }
`;

const AddTag = (props) => {
  const {
    form,
    visiableFormAddTag,
    setvisiableFormAddTag,
    refetchDataTag,
    dataTag,
    tagSelected,
  } = props;
  const { getFieldDecorator } = form;
  const [addTag] = useMutation(ADD_TAG);
  const [editTag] = useMutation(EDIT_TAG);
  console.log(tagSelected);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        if (!tagSelected) {
          addTag({ variables: { input: values } })
            .then(() => {
              form.resetFields();
              refetchDataTag();
              setvisiableFormAddTag(false);
              notification.success({
                message: 'Thêm kỹ năng thành công',
              });
            });
        } else {
          editTag({ variables: { tagID: tagSelected._id, input: values } }).then(() => {
            form.resetFields();
            refetchDataTag();
            setvisiableFormAddTag(false);
            notification.success({
              message: 'Sửa kỹ năng thành công',
            });
          });
        }
      }
    });
  };
  return (
    <div>
      <Drawer
        title="Thêm kỹ năng"
        width={500}
        onClose={() => {
          setvisiableFormAddTag(false);
          form.resetFields();
        }}
        visible={visiableFormAddTag}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form onSubmit={handleSubmit} layout="vertical" className="login-form">
          <Row gutter={16}>
            <Form.Item label="Tên">
              {getFieldDecorator('name', {
                initialValue: tagSelected ? tagSelected.name : '',
                rules: [
                  { required: true, message: 'Vui lòng nhập tên!' },
                  {
                    validator: (rule, value, callback) => {
                      if (tagSelected) {
                        callback();
                      }
                      const index = dataTag.findIndex((item) => item.name === value);
                      if (index !== -1) {
                        callback('Tên kỹ năng đã tồn tại');
                      } else {
                        callback();
                      }
                    },
                  },
                ],
              })(<Input />)}
            </Form.Item>
          </Row>
          <Row gutter={16}>
            <Form.Item label="Trạng thái">
              {getFieldDecorator('isAproved', {
                initialValue: tagSelected ? tagSelected.isAproved : null,
                rules: [{ required: true, message: 'Vui lòng chọn trạng thái!' }],
              })(
                <Select>
                  <Option value={true}>Đã duyệt</Option>
                  <Option value={false}>Chưa duyệt</Option>
                </Select>,
              )}
            </Form.Item>
          </Row>
        </Form>
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={() => setvisiableFormAddTag(false)} style={{ marginRight: 8 }}>
            Hủy
          </Button>
          <Button onClick={handleSubmit} type="primary">
            Lưu
            </Button>
        </div>
      </Drawer>
    </div >
  );
};

const FormAddTag = Form.create()(AddTag);

export default FormAddTag;
