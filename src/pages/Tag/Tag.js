/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
import React, { useState } from 'react';
import {
  Table, Button, Icon, Popconfirm, notification, Spin,
} from 'antd';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import MenuLeft from '../../components/MenuLeft/index';
import AddTag from './AddTag/AddTag';

const GET_TAGS = gql`
  query{
  tags{
    _id
    name
    isAproved
  }
}
`;

const DELETE_TAG = gql`
  mutation DeleteTag($tagID: ID!){
  deleteTag(tagID: $tagID)
}
`;

const Tag = () => {
  const { data: dataTag, refetch: refetchDataTag, loading } = useQuery(GET_TAGS);
  const [visiableFormAddTag, setvisiableFormAddTag] = useState(false);
  const [tagSelected, setTagSelected] = useState(null);
  const [deleteTag] = useMutation(DELETE_TAG);

  const handeDeleteTag = (tagID) => {
    deleteTag({ variables: { tagID } })
      .then(() => {
        refetchDataTag();
        notification.success({
          message: 'Xóa kỹ năng thành công',
        });
      });
  };

  const columns = [
    {
      title: 'Trạng thái',
      dataIndex: 'isAproved',
      width: 100,
      // eslint-disable-next-line arrow-body-style
      render: (text) => (
        <div style={{ textAlign: 'center' }}>
          {text ? <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" /> : <Icon type="close-circle" theme="twoTone" twoToneColor="#ff0000" />}
        </div>
      ),
    },
    {
      title: 'Tên',
      dataIndex: 'name',
    },
    // {
    //   title: 'Trạng thái',
    //   dataIndex: 'isAproved',
    //   // eslint-disable-next-line arrow-body-style
    //   render: (text) => (
    //     <div style={{ fontWeight: 'bold' }}>
    //       {text ? <span style={{ color: 'green' }}>Đã duyệt</span> : < span style={{ color: 'red' }}> Chưa duyệt</span >}
    //     </div >
    //   ),
    // },
    {
      title: 'Tắc vụ',
      key: 'action',
      // fixed: 'right',
      width: 100,
      render: (item) => (
        <>
          <Button type="primary" size="small" onClick={() => {
            setTagSelected(item);
            setvisiableFormAddTag(true);
          }}><Icon type="edit" /></Button>{' '}
          <Popconfirm
            title="Bạn có muốn xóa không?"
            onConfirm={() => handeDeleteTag(item._id)}
            // onCancel={cancel}
            okText="Có"
            cancelText="Không"
          >
            <Button type="danger" size="small"><Icon type="delete" /></Button>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <MenuLeft textSelected='tag'>
      {!loading ? (
        <Table
          columns={columns}
          dataSource={dataTag ? dataTag.tags : null}
          bordered
          rowKey='_id'
          title={() => (
            <h3>Kỹ năng <Button type="primary" onClick={() => {
              setvisiableFormAddTag(true);
              setTagSelected(null);
            }} icon="plus" style={{ float: 'right' }} /></h3>
          )}
          scroll={{ y: 410 }}
        />
      ) : <Spin />}
      <AddTag
        dataTag={dataTag ? dataTag.tags : null}
        visiableFormAddTag={visiableFormAddTag}
        setvisiableFormAddTag={setvisiableFormAddTag}
        refetchDataTag={refetchDataTag}
        tagSelected={tagSelected}
      />
    </MenuLeft>
  );
};

export default Tag;
