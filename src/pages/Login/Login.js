import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row, Col, Form, Icon, Input, Button, Card } from 'antd';

import * as actions from '../../tools/redux/actions';

const LOGIN = gql`
  mutation Login($input: LoginInput!) {
    login(input: $input) {
      token
      user {
        fullname
        email
      }
    }
  }
`;

const Login = (props) => {
  const { handleSaveUser, form } = props;
  const { getFieldDecorator } = form;
  const history = useHistory();
  const [login] = useMutation(LOGIN);

  const handleSubmitFormLogin = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        const { username, password } = values;
        login({
          variables: {
            input: { email: username, password },
          },
        })
          .then((req) => {
            handleSaveUser(req.data.login.user);
            localStorage.setItem('token', req.data.login.token);
            history.push('/');
          })
          .catch((er) => {
            console.log(er);
          });
      }
    });
  };

  return (
    <Row type="flex" justify="center" align="middle" style={{ height: '100vh', background: 'url(https://maucoklat.com/images/content/1558505892bg.png) no-repeat fixed center' }}>
      <Col span={6}>
        <Card title="Đăng nhập" headStyle={{ textAlign: 'center' }}>
          <Form onSubmit={handleSubmitFormLogin} className="login-form">
            <Form.Item>
              {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Vui lòng nhập tài khoản!' }],
              })(
                <Input
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Tài khoản"
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Vui lòng nhập mật khẩu!' }],
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  placeholder="Mật khẩu"
                />,
              )}
            </Form.Item>
            <Form.Item style={{ margin: 0 }}>
              <Button type="primary" htmlType="submit" className="login-form-button" block>
                Đăng nhập
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => dispatch(actions.saveUser(user)),
});

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
export default connect(null, mapDispatchToProps)(WrappedNormalLoginForm);
