import React, { useState, useEffect } from 'react';
import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';
import {
  Table, Tag, Spin,
} from 'antd';
import numeral from 'numeral';
import { isSameWeek, isSameMonth, isSameYear, isSameDay, addMonths } from 'date-fns/esm';
// eslint-disable-next-line import/no-named-as-default-member


const GET_CONTRACTs = gql`
    query  {
      contracts{
      teacher{
        _id
        email
        fullname
      }
      skill{
        name
      }
      feeHourly
      timeStudy
      state
      finishAt
  }
}
`;

// const getMonday = (d) => {
//   const mydate = new Date(d);
//   const day = mydate.getDay();
//   const diff = mydate.getDate() - day + (day === 0 ? -6 : 1); // adjust when day is sunday
//   return new Date(mydate.setDate(diff));
// }
const Details = (props) => {
  console.log('sdasdasdas');
  const { data: dataContracts } = useQuery(GET_CONTRACTs, { fetchPolicy: 'network-only' });
  const { date, skill } = props;
  const [top, setTop] = useState([]);


  // const FillterByDate = (type, dataDate) => {
  //   const mydate = new Date(+dataDate);
  //   if (type === 'ALL') {
  //     return true;
  //   }
  //   if (type === 'day') {
  //     if (isSameDay(mydate, now)) {
  //       return true;
  //     }
  //     return false;
  //   }
  //   if (type === 'month') {
  //     if (isSameMonth(mydate, now)) {
  //       return true;
  //     }
  //     return false;
  //   }
  //   if (type === '3month') {
  //     const month3 = addMonths(mydate, 3);
  //     if (month3 >= now) {
  //       return true;
  //     }
  //     return false;
  //   }
  //   if (type === 'year') {
  //     if (isSameYear(mydate, now)) {
  //       return true;
  //     }
  //     return false;
  //   }
  //   if (type === 'week') {
  //     if (isSameWeek(mydate, now)) {
  //       return true;
  //     }
  //     return false;
  //   }
  // };
  // const FillterBySkill = (tagskill, dataSkill) => {
  //   if (tagskill === 'ALL' || tagskill === dataSkill) {
  //     return true;
  //   }
  //   return false;
  // };

  // const arr = [];
  // const fillter = () => {
  //   // eslint-disable-next-line array-callback-return
  //   dataContracts.contracts.map((v) => {
  //     if (FillterBySkill(skill, v.skill.name)
  //  && FillterByDate(date, v.finishAt) && v.state !== 'PENDING') {
  //       let check = 0;
  //       for (let i = 0; i < arr.length; i++) {
  //         if (arr.length > 10) {
  //           break;
  //         }
  //         // eslint-disable-next-line no-underscore-dangle
  //         if (arr[i].id === v.teacher._id) {
  //           check = 1;
  //           const money = arr[i].sumMoney
  //             + parseInt(v.feeHourly, 10) * parseInt(v.timeStudy, 10);
  //           arr[i].sumMoney = money;
  //           let check2 = 0;
  //           for (let k = 0; k < arr[i].Skills.length; k++) {
  //             if (arr[i].Skills[k] === v.skill.name) {
  //               check2 = 1;
  //               break;
  //             }
  //           }
  //           if (check2 === 0) {
  //             arr[i].Skills.push(v.skill.name);
  //           }
  //           break;
  //         }
  //       }
  //       if (check === 0 && arr.length < 10) {
  //         const dataSkill = [];
  //         dataSkill.push(v.skill.name);
  //         arr.push({
  //           // eslint-disable-next-line no-underscore-dangle
  //           id: v.teacher._id,
  //           fullname: v.teacher.fullname,
  //           email: v.teacher.email,
  //           sumMoney: (parseInt(v.feeHourly, 10) * parseInt(v.timeStudy, 10)),
  //           Skills: dataSkill,
  //         });
  //       }
  //     }
  //   });
  //   // eslint-disable-next-line no-confusing-arrow
  //   arr.sort((a, b) => (a.sumMoney < b.sumMoney) ? 1 : -1);
  //   for (let i = 0; i < arr.length; i++) {
  //     arr[i].ViTri = i + 1;
  //     // eslint-disable-next-line prefer-template
  //     arr[i].sumMoney = numeral(arr[i].sumMoney).format('0,0[.]00') + ' VND';
  //   }
  //   setTop(arr);
  // };
  // if (dataContracts) {
  //   fillter();
  // }
  useEffect(() => {
    if (dataContracts) {
      const now = new Date(Date.now());
      const arr = [];
      const FillterByDate = (type, dataDate) => {
        const mydate = new Date(+dataDate);
        if (type === 'ALL') {
          return true;
        }
        if (type === 'day') {
          if (isSameDay(mydate, now)) {
            return true;
          }
          return false;
        }
        if (type === 'month') {
          if (isSameMonth(mydate, now)) {
            return true;
          }
          return false;
        }
        if (type === '3month') {
          const month3 = addMonths(mydate, 3);
          if (month3 >= now) {
            return true;
          }
          return false;
        }
        if (type === 'year') {
          if (isSameYear(mydate, now)) {
            return true;
          }
          return false;
        }
        if (type === 'week') {
          if (isSameWeek(mydate, now)) {
            return true;
          }
          return false;
        }
      };
      const FillterBySkill = (tagskill, dataSkill) => {
        if (tagskill === 'ALL' || tagskill === dataSkill) {
          return true;
        }
        return false;
      };
      // eslint-disable-next-line array-callback-return
      dataContracts.contracts.map((v) => {
        if (FillterBySkill(skill, v.skill.name) && FillterByDate(date, v.finishAt) && v.state !== 'PENDING') {
          let check = 0;
          for (let i = 0; i < arr.length; i++) {
            if (arr.length > 10) {
              break;
            }
            // eslint-disable-next-line no-underscore-dangle
            if (arr[i].id === v.teacher._id) {
              check = 1;
              const money = arr[i].sumMoney
                + parseInt(v.feeHourly, 10) * parseInt(v.timeStudy, 10);
              arr[i].sumMoney = money;
              let check2 = 0;
              for (let k = 0; k < arr[i].Skills.length; k++) {
                if (arr[i].Skills[k] === v.skill.name) {
                  check2 = 1;
                  break;
                }
              }
              if (check2 === 0) {
                arr[i].Skills.push(v.skill.name);
              }
              break;
            }
          }
          if (check === 0 && arr.length < 10) {
            const dataSkill = [];
            dataSkill.push(v.skill.name);
            arr.push({
              // eslint-disable-next-line no-underscore-dangle
              id: v.teacher._id,
              fullname: v.teacher.fullname,
              email: v.teacher.email,
              sumMoney: (parseInt(v.feeHourly, 10) * parseInt(v.timeStudy, 10)),
              Skills: dataSkill,
            });
          }
        }
      });
      // eslint-disable-next-line no-confusing-arrow
      arr.sort((a, b) => (a.sumMoney < b.sumMoney) ? 1 : -1);
      for (let i = 0; i < arr.length; i++) {
        arr[i].ViTri = i + 1;
        // eslint-disable-next-line prefer-template
        arr[i].sumMoney = numeral(arr[i].sumMoney).format('0,0[.]00') + ' VND';
      }
      setTop(arr);
    }
  }, [dataContracts, skill, date]);
  const columns = [
    {
      title: 'Thứ hạng',
      dataIndex: 'ViTri',
      render: (text) => (
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          <Tag color="#f50" key={text}>
            {text}
          </Tag>
        </div>
      ),
    }, {
      title: 'Họ tên',
      dataIndex: 'fullname',
      width: 150,
      // eslint-disable-next-line arrow-body-style
      render: (text) => (
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          <Tag color="#2db7f5" key={text}>
            {text.toUpperCase()}
          </Tag>
        </div>
      ),
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Thu nhập',
      dataIndex: 'sumMoney',
    }, {
      title: 'Kỹ năng',
      dataIndex: 'Skills',
      render: (text) => (
        <div>
          {text.map((value) => (
            <Tag color="volcano" key={value}>
              {value}
            </Tag>
          ))}
        </div>
      ),
    },
  ];
  return (
    <div >
      {dataContracts ? (
        <Table
          columns={columns}
          dataSource={top}
          bordered
          rowKey='_id'
          title={() => (
            <h3>BẢNG XẾP HẠNG THU NHẬP GIÁO VIÊN</h3>
          )}
          scroll={{ y: 500 }}
        />
      )
        : <Spin />
      }
    </div >
  );
};

export default Details;
