/* eslint-disable import/no-named-as-default */
import React, { useState } from 'react';
import { Card, Select, Spin } from 'antd';
import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';
// eslint-disable-next-line import/no-named-as-default-member
import MenuLeft from '../../components/MenuLeft/index';
import Details from './Details';

const GET_TAGS = gql`
    query  {
      tags{
      name
      _id
      isAproved
     }
}
`;
const { Option } = Select;
const Index = () => {
  const [date, setDate] = useState('month');
  const [skill, setSkill] = useState('ALL');
  const { loading: loaddingTags, data: dataTags } = useQuery(GET_TAGS, { fetchPolicy: 'network-only' });
  console.log(loaddingTags, dataTags);
  const ChangeData = (value) => {
    setDate(value);
  };
  const ChangeSkill = (value) => {
    setSkill(value);
  };
  return (
    <MenuLeft textSelected='toprevenue'>
      {!loaddingTags ? (
        <div>
          <Card title="Thống kê doanh thu" bordered={false} extra={
            <div>
              <div style={{ display: 'inline-block', marginRight: '10px' }}>
                <span>Thời gian: </span>
                <Select defaultValue="month" style={{ width: 120 }} onChange={ChangeData}>
                  <Select.Option value="day">Ngày</Select.Option>
                  <Select.Option value="week">Tuần</Select.Option>
                  <Select.Option value="month">30 Ngày</Select.Option>
                  <Select.Option value="3month">90 Ngày</Select.Option>
                  <Select.Option value="ALL">Tất cả</Select.Option>
                </Select>
              </div>
              <div style={{ display: 'inline-block' }}>
                <span>Kỹ năng: </span>
                <Select defaultValue="ALL" style={{ width: 120 }} onChange={ChangeSkill}>
                  <Option value="ALL">Tất cả</Option>
                  {dataTags.tags.map((value, idx) => <Option
                    key={idx} value={value.name}>{value.name}</Option>)}
                </Select>
              </div>
            </div>
          }>
            <Details date={date} skill={skill} />
          </Card>
        </div>
      ) : (
          <Spin />
          // eslint-disable-next-line indent
        )}

    </MenuLeft>
  );
};

export default Index;
