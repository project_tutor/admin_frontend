/* eslint-disable max-len */
import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import { Spin, Card, Select } from 'antd';
import { withApollo } from 'react-apollo';
import { gql } from 'apollo-boost';
import MenuLeft from '../../components/MenuLeft/index';

const GET_MY_CONTRACTS = gql`
  query contracts {
    contracts {
    _id
    feeHourly
    timeStudy
    state
    finishAt
  }
}
`;

class Charts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myContracts: [],
      loading: false,
      data: [],
      dataDay: [],
      dataWeek: [],
      dataMonth: [],
      dataYear: [],
      type: 'month',
    };

    this.GetContractData = this.GetContractData.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
  }

  InitData() {
    const { dataDay, dataMonth, dataWeek } = this.state;

    const now = new Date();
    for (let i = 1; i <= now.getDate() + 7; i++) {
      dataDay.push({
        name: `${i < 10 ? `0${i}` : i}/${now.getMonth() + 1}`,
        money: 0,
      });
    }
    for (let i = 1; i <= now.getMonth() + 1; i++) {
      dataMonth.push({
        name: `${i < 10 ? `0${i}` : i}/${now.getFullYear()}`,
        money: 0,
      });
    }
    for (let i = 1; i <= (now.getDay()); i++) {
      dataWeek.push({
        name: i < 7 ? `Thứ ${i + 1}` : 'Chủ nhật',
        money: 0,
      });
    }
  }

  componentWillMount() {
    this.InitData();
    const { loading } = this.state;
    if (!loading) {
      this.setState({
        loading: !loading,
      }, () => {
        const data = [];
        const now = new Date();
        for (let i = 1; i <= now.getMonth() + 1; i++) {
          data.push({
            name: `${i < 10 ? `0${i}` : i}/${now.getFullYear()}`,
            money: 0,
          });
        }
        this.setState({ data }, () => {
          this.GetContractData();
        });
      });
    }
  }

  GetContractData() {
    const { client } = this.props;
    client.query({
      query: GET_MY_CONTRACTS,
    }).then((res) => {
      this.setState({
        myContracts: res.data.contracts,
      }, () => {
        this.GetIncomeData();
      });
    }).catch((err) => {
      console.log(err);
    });
  }

  GetIncomeData() {
    const { myContracts, dataMonth, dataYear, dataDay, dataWeek } = this.state;
    const tempData = dataMonth.slice(0);
    const tempDataYear = dataYear.slice(0);
    const tempDataDay = dataDay.slice(0);
    const tempDataWeek = dataWeek.slice(0);
    console.log(tempData);
    myContracts.forEach((el) => {
      if (el.state === 'FINISH') {
        const date = new Date(+el.finishAt);
        const now = new Date();
        tempData[date.getMonth()].money += parseInt(el.feeHourly, 0) * el.timeStudy;
        const index = this.CheckIsExistedData(date.getFullYear().toString(), tempDataYear);
        if (index !== -1) {
          tempDataYear[index].money += parseInt(el.feeHourly, 0) * el.timeStudy;
        } else {
          tempDataYear.push({ name: date.getFullYear().toString(), money: parseInt(el.feeHourly, 0) * el.timeStudy });
        }
        tempDataDay[date.getDate() - 1].money += parseInt(el.feeHourly, 0) * el.timeStudy;
        if (new Date(+now.setDate(now.getDate() - (now.getDay() === 0 ? 7 : now.getDay()) + 1)).getDate()
        === new Date(+date.setDate(date.getDate() - (date.getDay() === 0 ? 7 : date.getDay()) + 1)).getDate()) {
          tempDataWeek[date.getDay() === 0 ? 6 : date.getDay() - 1].money += parseInt(el.feeHourly, 0) * el.timeStudy;
        }
      }
    });
    this.setState({ dataMonth: tempData, loading: false, dataYear: tempDataYear, data: tempData, dataDay: tempDataDay });
  }


  // eslint-disable-next-line class-methods-use-this
  CheckIsExistedData(name, data) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].name === name) {
        return i;
      }
    }
    return -1;
  }


  handleChangeSelect(value) {
    const { dataDay, dataWeek, dataMonth, dataYear } = this.state;
    switch (value) {
      case 'day':
        this.setState({ data: dataDay });
        break;
      case 'week':
        this.setState({ data: dataWeek });
        break;
      case 'month':
        this.setState({ data: dataMonth });
        break;
      case 'year':
        this.setState({ data: dataYear });
        break;
      default:
        break;
    }
    this.setState({ type: value });
  }


  render() {
    const { data, loading } = this.state;
    const mselect = (
        <div>
            <span>Thống kê theo: </span>
            <Select defaultValue="month" style={{ width: 120, marginLeft: 20 }} onChange={this.handleChangeSelect}>
                <Select.Option value="day">Ngày</Select.Option>
                <Select.Option value="week">Tuần</Select.Option>
                <Select.Option value="month">Tháng</Select.Option>
                <Select.Option value="year">Năm</Select.Option>
            </Select>
        </div>
    );

    return (<MenuLeft textSelected='charts'>
                    <Card title="Thống kê doanh thu" bordered={false} extra={mselect}>
                        {loading ? <Spin /> : <LineChart width={1500} height={600} data={data}
                            margin={{ top: 5, right: 30, left: 20, bottom: 15 }}>
                            <CartesianGrid strokeDasharray="5 5" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend verticalAlign="top" height={36}/>
                            <Line type="monotone" dataKey="money" stroke="#82ca9d" dot={{ stroke: 'red', strokeWidth: 2 }} />
                        </LineChart>}
                    </Card>
            </MenuLeft>);
  }
}

export default withApollo(Charts);
