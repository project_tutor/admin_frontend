import React, { useState } from 'react';
import {
  Table, Tag, Button, Icon, Modal, Spin,
} from 'antd';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import MenuLeft from '../../components/MenuLeft/index';
import ContractDetail from './ContractDetail';


const GET_COMPLAINTS = gql`
  query contracts {
  contracts {
    _id
    state
    teacher {
      email
      fullname
      phone
      imageURL
      location {
        address
        city
      }
    }
    student {
      email
      fullname
      phone
      imageURL
      location {
        address
        city
      }
    }
    title
    description 
    feeHourly
    timeStudy
    skill {
      name
    }
  }
}
`;

function getColorTag(text) {
  switch (text) {
    case 'PENDING':
      return 'GRAY';
    case 'AGREE':
      return 'LIME';
    case 'DISAGREE':
      return 'RED';
    case 'FINISH':
      return 'BLUE';
    case 'COMPLAINING':
      return 'OLIVE';
    default:
      break;
  }
}

function Contract() {
  const [visibaleModal, setVisibaleModal] = useState(false);
  const [contractSelected, setContractSelected] = useState(null);
  const { loading: loadingDataComplaints, data: dataComplaints } = useQuery(GET_COMPLAINTS);

  const columns = [
    {
      title: 'Trạng thái',
      dataIndex: 'state',
      width: 150,
      // eslint-disable-next-line arrow-body-style
      render: (text) => (
        <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
          <Tag color={getColorTag(text)} key={text}>
            {text.toUpperCase()}
          </Tag>
        </div>
      ),
    },
    {
      title: 'Tên hợp đồng',
      dataIndex: 'title',
    },
    {
      title: 'Giáo Viên',
      dataIndex: 'teacher.fullname',
    },
    {
      title: 'Học sinh',
      dataIndex: 'student.fullname',
    },
    {
      title: 'Môn học',
      dataIndex: 'skill.name',
    },
    {
      title: 'Tắc vụ',
      key: 'action',
      // fixed: 'right',
      width: 100,
      render: (item) => (
        <>
          <Button type="primary" size="small" onClick={() => {
            setVisibaleModal(true);
            setContractSelected(item);
          }}>
            <Icon type="eye" />
          </Button>
        </>
      ),
    },
  ];

  return (
    <MenuLeft textSelected='contract'>
      {!loadingDataComplaints ? (
        <Table
          columns={columns}
          dataSource={dataComplaints ? dataComplaints.contracts : null}
          bordered
          rowKey='_id'
          title={() => (
            <h3>Hợp đồng</h3>
          )}
          scroll={{ y: 410 }}
        />
      ) : <Spin />}

      <Modal
        title="Chi tiết hợp đồng"
        visible={visibaleModal}
        width={'60%'}
        onOk={() => setVisibaleModal(false)}
        onCancel={() => setVisibaleModal(false)}
        footer={
          <div style={{ color: 'gray', textAlign: 'center' }}>(C) Bản quyền thuộc về UberTutor</div>
        }
      >
        <ContractDetail
          contractData={contractSelected}
          setVisibaleModal={setVisibaleModal}
        />
      </Modal>
    </MenuLeft>
  );
}

export default Contract;
