import React from 'react';
import {
  Layout, Row, Col, Avatar, Icon, Input,
  Divider, Timeline, Form, InputNumber,
} from 'antd';

const url_backend = 'https://tutor-client-backend.herokuapp.com';

function ContractDetail(props) {
  const { contractData } = props;
  const { teacher, student, title, description, skill, feeHourly, timeStudy } = contractData;
  const { fullname, location, phone, imageURL, email } = teacher;
  const { getFieldDecorator } = props.form;

  return (
    <Layout className="Detail-profile" style={{ background: '#FFFFFF' }}>
      <Form>
        <Row style={{ display: 'block' }}>
          <Form.Item label={'Tên hợp đồng:'}>
            {getFieldDecorator('title', {
              initialValue: title,
              rules: [
                {
                  required: true,
                  message: 'Tên hợp đồng không được bỏ trống!',
                },
              ],
            })(<Input readOnly />)}
          </Form.Item>
        </Row>
        <Divider style={{}} orientation="center">Thông tin</Divider>
        <Row style={{ background: 'white', overflow: 'hidden', display: 'flex', flexDirection: 'row' }}>
          <Col span={12} >
            <Row ><h1 style={{ color: 'red' }}> Giáo viên</h1></Row>
            <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Avatar src={url_backend + imageURL} size={150}></Avatar>
              <h1 style={{ marginTop: 5, color: 'green' }}>{fullname}</h1>
            </Row>
            <Divider orientation="left">Liên hệ</Divider>

            <Row style={{}}>
              <div>
                <Icon type="home" />
                <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${location.address}, ${location.city}`} </div>
              </div>
              <div>
                <Icon type="phone" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{phone}</p>
              </div>
              <div>
                <Icon type="mail" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{email}</p>
              </div>
            </Row>

          </Col>

          <Timeline>
            <Timeline.Item style={{ height: '100%' }}></Timeline.Item>
            <Timeline.Item></Timeline.Item>
          </Timeline>
          <Col span={13}>
            <Row ><h1 style={{ color: 'red' }}> Học sinh</h1></Row>

            <Row style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Avatar src={url_backend + student.imageURL} size={150}></Avatar>
              <h1 style={{ marginTop: 5, color: 'green' }}>{student.fullname}</h1>
            </Row>
            <Divider orientation="left">Liên hệ</Divider>

            <Row style={{}}>
              <div>
                <Icon type="home" />
                <div style={{ marginLeft: 10, padding: 'unset', display: 'inline' }} >{`${student.location.address}, ${student.location.city}`} </div>
              </div>
              <div>
                <Icon type="phone" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{student.phone}</p>
              </div>
              <div>
                <Icon type="mail" />
                <p style={{ marginLeft: 10, display: 'inline' }} >{`${student.email}$/hrs`}</p>
              </div>
            </Row>
          </Col>
        </Row>
        <Divider style={{ marginTop: 50 }} orientation="center">Mô tả</Divider>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Mô tả hợp đồng: </b></h3>
          <Form.Item>
            {getFieldDecorator('description', {
              rules: [
                {
                  required: true,
                  message: 'Mô tả không được bỏ trống!',
                },
              ],
            })(
              <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }}>
                <Input.TextArea rows={8} readOnly value={description} />
              </div>,
            )}
          </Form.Item>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Chọn môn học: </b></h3>
          <Form.Item label={'Tên hợp đồng:'}>
            {getFieldDecorator('skill', {
              initialValue: skill.name,
              rules: [
                {
                  required: true,
                  message: 'Tên hợp đồng không được bỏ trống!',
                },
              ],
            })(<Input readOnly />)}
          </Form.Item>
        </Row>
        <Divider style={{ marginTop: 50 }} orientation="center">Thanh toán</Divider>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Tiền thuê trên giờ(VNĐ/Giờ): </b></h3>
          <Form.Item>
            {getFieldDecorator('feeHourly', {
              initialValue: feeHourly,
            })(
              <InputNumber
                formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                style={{ width: 100 }}
                min={1}
                className="mInputHourly"
                readOnly
              />,
            )}
          </Form.Item>
        </Row>
        <Row style={{ marginTop: 20 }}>
          <h3><b>Số Giờ Thuê(Giờ): </b></h3>
          <Form.Item>
            {getFieldDecorator('timeStudy', {
              initialValue: timeStudy,
            })(
              <InputNumber
                formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                parser={(value) => value.replace(/\$\s?|(,*)/g, '').replace('H', '')}
                min={1}
                readOnly
              />,
            )}
          </Form.Item>
        </Row>
        <Row>
          <h3 style={{ color: 'red' }}><b>{`Tổng số tiền : ${(timeStudy * feeHourly).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')} VNĐ`}</b></h3>
        </Row>
      </Form>
    </Layout >
  );
}


export default Form.create({ name: 'ContractDetail' })(ContractDetail);
