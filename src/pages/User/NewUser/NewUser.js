import React from 'react';
import { Drawer, Form, Button, Row, Input, Select, notification } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const { Option } = Select;

const ADD_USER = gql`
  mutation AddUser($input: UserInputAdd!) {
    addUser(input: $input)
  }
`;


const NewUser = (props) => {
  const { form, visiableFormNewUser, setvisiableFormNewUser, refetchdataUser, dataUser } = props;
  const { getFieldDecorator } = form;
  const [addUser] = useMutation(ADD_USER);
  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        addUser({ variables: { input: values } })
          .then(() => {
            form.resetFields();
            refetchdataUser();
            setvisiableFormNewUser(false);
            notification.success({
              message: 'Thêm tài khoản thành công',
            });
          });
      }
    });
  };

  const checkEmailExist = (rule, value, callback) => {
    const index = dataUser.findIndex((item) => item.email === value);
    if (index !== -1) {
      callback('Email đã tồn tại!');
    } else {
      callback();
    }
  };
  return (
    <div>
      <Drawer
        title="Thêm tài khoản"
        width={500}
        onClose={() => {
          setvisiableFormNewUser(false);
          form.resetFields();
        }}
        visible={visiableFormNewUser}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form onSubmit={handleSubmit} layout="vertical" className="login-form">
          <Row gutter={16}>
            <Form.Item label="Họ tên">
              {getFieldDecorator('fullname', {
                rules: [{ required: true, message: 'Vui lòng nhập họ tên!' }],
              })(<Input />)}
            </Form.Item>
          </Row>
          <Row gutter={16}>
            <Form.Item label="Email">
              {getFieldDecorator('email', {
                rules: [
                  { type: 'email', message: 'Email không hợp lệ!' },
                  { validator: checkEmailExist },
                  { required: true, message: 'Vui lòng nhập email!' },
                ],
              })(<Input />)}
            </Form.Item>
          </Row>
          <Row gutter={16}>
            <Form.Item label="Mật khẩu">
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Vui lòng nhập mật khẩu!' }],
              })(<Input />)}
            </Form.Item>
          </Row>
          <Row gutter={16}>
            <Form.Item label="Loại tài khoản">
              {getFieldDecorator('role', {
                rules: [{ required: true, message: 'Vui lòng chọn loại tài khoản!' }],
              })(
                <Select>
                  <Option value="STUDENT">Học sinh</Option>
                  <Option value="TEACHER">Giáo viên</Option>
                  <Option value="ADMIN">Quản trị</Option>
                </Select>,
              )}
            </Form.Item>
          </Row>
          {/* <Row gutter={16}>
            <Col span={12}>
              <Form.Item label="Owner">
                {getFieldDecorator('owner', {
                  rules: [{ required: true, message: 'Please select an owner' }],
                })(
                  <Select placeholder="Please select an owner">
                    <Option value="xiao">Xiaoxiao Fu</Option>
                    <Option value="mao">Maomao Zhou</Option>
                  </Select>,
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Type">
                {getFieldDecorator('type', {
                  rules: [{ required: true, message: 'Please choose the type' }],
                })(
                  <Select placeholder="Please choose the type">
                    <Option value="private">Private</Option>
                    <Option value="public">Public</Option>
                  </Select>,
                )}
              </Form.Item>
            </Col>
          </Row> */}
        </Form>
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={() => setvisiableFormNewUser(false)} style={{ marginRight: 8 }}>
            Hủy
          </Button>
          <Button onClick={handleSubmit} type="primary">
            Xác nhận
            </Button>
        </div>
      </Drawer>
    </div >
  );
};

const FormNewUser = Form.create()(NewUser);

export default FormNewUser;
