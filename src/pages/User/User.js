/* eslint-disable no-underscore-dangle */
import React, { useState } from 'react';
import { Table, Button, Icon, Popconfirm, notification, Spin } from 'antd';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import MenuLeft from '../../components/MenuLeft/index';
import NewUser from './NewUser/NewUser';
import EditUser from './EditUser/EditUser';

const GET_USERS = gql`
  query {
    users {
      _id
      fullname
      email
      role
      isLocked
    }
  }
`;

const DELETE_USERS = gql`
  mutation DeleteUser($userID: ID!) {
    deleteUser(userID: $userID) 
  }
`;

const User = () => {
  const { data: dataUser, refetch: refetchdataUser, loading } = useQuery(GET_USERS);
  const [deleteUser] = useMutation(DELETE_USERS);
  const [visiableFormNewUser, setvisiableFormNewUser] = useState(false);
  const [visiableFormEditUser, setvisiableFormEditUser] = useState(false);
  const [userSelected, setUserSelected] = useState(null);

  // if (loading) return <p>Loading...</p>;
  // if (error) return <p>Error :(</p>;

  const handeEditUser = (item) => {
    setUserSelected(item);
    setvisiableFormEditUser(true);
  };
  const handeDeleteUser = (userID) => {
    deleteUser({ variables: { userID } })
      .then(() => {
        refetchdataUser();
        notification.success({
          message: 'Xóa tài khoản thành công',
        });
      });
  };

  const columns = [
    {
      title: 'Họ tên',
      dataIndex: 'fullname',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Loại tài khoản',
      dataIndex: 'role',
      render: (text) => {
        if (text === 'STUDENT') return 'Học Sinh';
        if (text === 'TEACHER') return 'Giáo viên';
        if (text === 'ADMIN') return 'Quản trị';
      },
    },
    {
      title: 'Trạng thái',
      dataIndex: 'isLocked',
      render: (text) => {
        // eslint-disable-next-line curly
        if (text === false) return <div>
          <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
          <span>     </span>Hoạt động
        </div>;
        // eslint-disable-next-line curly
        if (text === true) return <div>
          <Icon type="minus-circle" theme="twoTone" twoToneColor="#f5222d" />
          <span>     </span>Tạm khóa
        </div>;
      },
    },
    {
      title: 'Tắc vụ',
      key: 'action',
      // fixed: 'right',
      width: 200,
      render: (item) => (
        <>
          <Button type="primary" size="small" onClick={() => handeEditUser(item)}><Icon type="edit" /></Button>{' '}
          <Popconfirm
            title="Bạn có muốn xóa không?"
            onConfirm={() => handeDeleteUser(item._id)}
            // onCancel={cancel}
            okText="Có"
            cancelText="Không"
          >
            <Button type="danger" size="small"><Icon type="delete" /></Button>
          </Popconfirm>
        </>
      ),
    },
  ];

  return (
    <MenuLeft textSelected='home'>
      {loading ? <Spin /> : <div>
        <Table
          columns={columns}
          dataSource={dataUser ? dataUser.users : null}
          bordered
          rowKey='_id'
          title={() => (
            <h3>Tài khoản <Button type="primary" onClick={() => setvisiableFormNewUser(true)} icon="plus" style={{ float: 'right' }} /></h3>
          )}
          scroll={{ y: 410 }}
        />
        <NewUser
          dataUser={dataUser ? dataUser.users : null}
          visiableFormNewUser={visiableFormNewUser}
          setvisiableFormNewUser={setvisiableFormNewUser}
          refetchdataUser={refetchdataUser}
        />
        <EditUser
          visiableFormEditUser={visiableFormEditUser}
          setvisiableFormEditUser={setvisiableFormEditUser}
          refetchdataUser={refetchdataUser}
          userSelected={userSelected}
        />
      </div>}
    </MenuLeft>
  );
};

export default User;
