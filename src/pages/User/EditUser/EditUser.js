/* eslint-disable no-underscore-dangle */
import React from 'react';
import { Drawer, Form, Button, Row, Input, Select, notification } from 'antd';
import { useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const { Option } = Select;

const EDIT_USER = gql`
  mutation EditUser($userID: ID!, $input: UserInputEdit!) {
    editUser(userID: $userID, input:$input) 
  }
`;


const EditUser = (props) => {
  const {
    form,
    visiableFormEditUser,
    setvisiableFormEditUser,
    refetchdataUser,
    userSelected,
  } = props;
  const { getFieldDecorator } = form;
  const [editUser] = useMutation(EDIT_USER);

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        // console.log('Received values of form: ', values);
        editUser({
          variables: {
            userID: userSelected._id,
            input: {
              fullname: values.fullname,
              role: values.role,
              isLocked: values.isLocked,
            },
          },
        })
          .then(() => {
            form.resetFields();
            refetchdataUser();
            setvisiableFormEditUser(false);
            notification.success({
              message: 'Sửa tài khoản thành công',
            });
          });
      }
    });
  };

  return (
    <div>
      <Drawer
        title="Sửa tài khoản"
        width={500}
        onClose={() => setvisiableFormEditUser(false)}
        visible={visiableFormEditUser}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form onSubmit={handleSubmit} layout="vertical" className="login-form">
          <Row gutter={16}>
            <Form.Item label="Họ tên">
              {getFieldDecorator('fullname', {
                initialValue: userSelected ? userSelected.fullname : null,
                rules: [{ required: true, message: 'Vui lòng nhập họ tên!' }],
              })(<Input />)}
            </Form.Item>
          </Row>
          <Row gutter={16}>
            <Form.Item label="Loại tài khoản">
              {getFieldDecorator('role', {
                initialValue: userSelected ? userSelected.role : null,
                rules: [{ required: true, message: 'Vui lòng chọn loại tài khoản!' }],
              })(
                <Select>
                  <Option value="STUDENT">Học sinh</Option>
                  <Option value="TEACHER">Giáo viên</Option>
                  <Option value="ADMIN">Quản trị</Option>
                </Select>,
              )}
            </Form.Item>
          </Row>
          <Row gutter={16}>
            <Form.Item label="Mở/Khóa tài khoản">
              {getFieldDecorator('isLocked', {
                initialValue: userSelected ? userSelected.isLocked : null,
                rules: [{ required: true, message: '' }],
              })(
                <Select>
                  <Option value={true}>Tạm Khóa</Option>
                  <Option value={false}>Mở </Option>
                </Select>,
              )}
            </Form.Item>
          </Row>
        </Form>
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={() => setvisiableFormEditUser(false)} style={{ marginRight: 8 }}>
            Hủy
          </Button>
          <Button onClick={handleSubmit} type="primary">
            Xác nhận
            </Button>
        </div>
      </Drawer>
    </div >
  );
};

const FormEditUser = Form.create()(EditUser);

export default FormEditUser;
