import * as types from '../constants';

const initialState = null;

export default function reducer(state = initialState, action) {
  let user = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case types.SAVE_USER:
      user = action.user;
      return user;
    default:
      return state;
  }
}
