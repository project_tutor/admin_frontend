import * as types from './constants';

/**
|--------------------------------------------------
| User
|--------------------------------------------------
*/

export const saveUser = (user) => ({
  type: types.SAVE_USER,
  user,
});
