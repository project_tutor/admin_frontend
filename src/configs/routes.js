import React from 'react';

import Login from '../pages/Login/Login';
import User from '../pages/User/User';
import Tag from '../pages/Tag/Tag';
import Contract from '../pages/Contract/Contract';
import NotFound from '../pages/NotFound/NotFound';
import Complaint from '../pages/Complaint/ComPlaint';
import Charts from '../pages/Chart/Charts';
import TopTevenue from '../pages/TopTevenue/Index';

/**
|--------------------------------------------------
| auth: -1 unlogin, 0 : unlogin && logined, 1: logined
|--------------------------------------------------
*/

export default [
  {
    path: '/',
    exact: true,
    auth: 1,
    redirect: '/login',
    component: () => <User />,
  },
  {
    path: '/login',
    exact: false,
    auth: -1,
    redirect: '/',
    component: () => <Login />,
  },
  {
    path: '/tag',
    exact: false,
    auth: 1,
    redirect: '/login',
    component: () => <Tag />,
  },
  {
    path: '/complaint',
    exact: false,
    auth: 1,
    redirect: '/login',
    component: () => <Complaint />,
  }, {
    path: '/toprevenue',
    exact: false,
    auth: 1,
    redirect: '/login',
    component: () => <TopTevenue />,
  }, {
    path: '/contract',
    exact: false,
    auth: 1,
    redirect: '/login',
    component: () => <Contract />,
  },
  {
    path: '/charts',
    exact: false,
    auth: 1,
    redirect: '/login',
    component: () => <Charts />,
  },
  {
    path: '*',
    exact: false,
    auth: 0,
    redirect: null,
    component: () => <NotFound />,
  },
];
