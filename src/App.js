import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

import routes from './configs/routes';
import * as actions from './tools/redux/actions';

import 'antd/dist/antd.css';

const USER_LOGIN = gql`
  query userLogin {
    userLogin {
      _id
      fullname
      email
    }
  }
`;

const App = (props) => {
  const { user, handleSaveUser } = props;
  const { loading, error, data: dataUser } = useQuery(USER_LOGIN, {
    fetchPolicy: 'network-only',
  });

  if (loading || error) return <></>;
  let logined = !!(dataUser.userLogin !== null || user !== null);
  if (user === null && dataUser.userLogin !== null && !localStorage.getItem('token')) {
    logined = false;
  } else {
    // eslint-disable-next-line no-lonely-if
    if (!user) {
      handleSaveUser(dataUser.userLogin);
    }
  }


  return (
    <Router>
      <Switch>
        {routes.map((item) => (
          <Route exact={item.exact} path={item.path} key={item.path}>
            {item.auth === 0
              || (item.auth === 1 && logined === true)
              || (item.auth === -1 && logined === false) ? (
                item.component()
              ) : (
                <Redirect
                  to={{
                    pathname: item.redirect ? item.redirect : 'not-found',
                  }}
                />
              )}
          </Route>
        ))}
      </Switch>
    </Router>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  handleSaveUser: (user) => {
    dispatch(actions.saveUser(user));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
