module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    // 'eslint:recommended',
    // 'plugin:react/recommended',
    'airbnb-base'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: ['react'],
  rules: {
    'react/jsx-uses-react': 1,
    'react/jsx-uses-vars': 1,
    'react/react-in-jsx-scope': 1,
    'no-plusplus': 0,
    'react/no-array-index-key': 0,
    'no-console': 0,
    'camelcase': 0,
    'consistent-return': 0,
    'import/prefer-default-export': 0,
    'object-curly-newline': 0,
    'linebreak-style': 0
  }
};
